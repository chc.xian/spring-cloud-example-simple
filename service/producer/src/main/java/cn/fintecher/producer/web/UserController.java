package cn.fintecher.producer.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ChenChang on 2018/8/21.
 */
@RestController
@RequestMapping("/api/user")
@Api(value = "用户", description = "用户")
public class UserController {
    private final Logger log = LoggerFactory.getLogger(UserController.class);

    @ApiOperation(value = "查询所有用户", notes = "查询所有用户")
    @GetMapping("/getAll")
    public ResponseEntity<List<Map<String, String>>> getAll() {
        log.debug("获取所有用户");
        List<Map<String, String>> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Map<String, String> user = new HashMap<>();
            user.put(String.valueOf(i), "用户" + i);
            users.add(user);
        }
        return ResponseEntity.ok().body(users);
    }

}
