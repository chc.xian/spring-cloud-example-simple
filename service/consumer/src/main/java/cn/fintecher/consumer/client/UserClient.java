package cn.fintecher.consumer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

/**
 * Created by ChenChang on 2018/8/21.
 */
@FeignClient("producer-service")
public interface UserClient {
    @RequestMapping(method = RequestMethod.GET, value = "/api/user/getAll")
    ResponseEntity<List<Map<String, String>>> getAll();
}
