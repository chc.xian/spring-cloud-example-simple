package cn.fintecher.consumer.web;

import cn.fintecher.consumer.client.UserClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ChenChang on 2018/8/21.
 */
@RestController
@RequestMapping("/api/consumer/user")
@Api(value = "用户消费者", description = "用户消费者")
public class UserController {
    private final Logger log = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserClient userClient;
    @Autowired
    private RestTemplate restTemplate;

    @ApiOperation(value = "查询所有用户", notes = "查询所有用户")
    @GetMapping("/getAllByFeign")
    public ResponseEntity<List<Map<String, String>>> getAllByFeign() {

        return userClient.getAll();
    }

    @ApiOperation(value = "查询所有用户", notes = "查询所有用户")
    @GetMapping("/getAllByRibbon")
    public ResponseEntity<String> getAllByRibbon() {
        String json = restTemplate.getForObject("http://producer-service/api/user/getAll", String.class);
        return ResponseEntity.ok(json);
    }
}
